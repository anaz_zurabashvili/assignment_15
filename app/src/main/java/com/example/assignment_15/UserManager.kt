package com.example.assignment_15

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.map


private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class UserManager(context: Context?) {

    private val dataStore: DataStore<Preferences> = context!!.dataStore

    companion object {

        val USER_FIRST_NAME = stringPreferencesKey("USER_FIRST_NAME")
        val USER_LAST_NAME = stringPreferencesKey("USER_LAST_NAME")
        val USER_AGE = intPreferencesKey("USER_AGE")
        val USER_EMAIL = stringPreferencesKey("USER_EMAIL")
        val USER_PROFILE_IMAGE = stringPreferencesKey("USER_PROFILE_IMAGE")
        val USER_GENDER = stringPreferencesKey("USER_GENDER")
        val USER_ADDRESS = stringPreferencesKey("USER_ADDRESS")
        val USER_PHONE_NUMBER = stringPreferencesKey("USER_NUMBER")

    }

    suspend fun storeUser(user: User) {
        dataStore.edit {
            it[USER_FIRST_NAME] = user.firstName.toString()
            it[USER_LAST_NAME] = user.lastName.toString()
            it[USER_AGE] = user.age.toString().toInt()
            it[USER_EMAIL] = user.email.toString()
            it[USER_PROFILE_IMAGE] = user.profileImage.toString()
            it[USER_GENDER] = user.gender.toString()
            it[USER_ADDRESS] = user.address.toString()
            it[USER_PHONE_NUMBER] = user.phoneNumber.toString()
        }
    }

    fun getFromDataStore() = dataStore.data.map {
        User(
            firstName = it[USER_FIRST_NAME] ?: "George",
            lastName = it[USER_LAST_NAME] ?: "Smith",
            email = it[USER_EMAIL] ?: "gsmith@gmail.com",
            age = it[USER_AGE] ?: 0,
            profileImage = it[USER_PROFILE_IMAGE]
                ?: "https://upload.wikimedia.org/wikipedia/commons/5/54/George_Smith-Assyriologist_001.jpg",
            address = it[USER_ADDRESS] ?: "Digomi, Tbilisi, Georgia",
            gender = it[USER_GENDER] ?: "Male",
            phoneNumber = it[USER_PHONE_NUMBER] ?: "+995 556 092 100"
        )
    }
}