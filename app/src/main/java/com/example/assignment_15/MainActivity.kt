package com.example.assignment_15

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.datastore.dataStoreFile
import com.example.assignment_15.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}