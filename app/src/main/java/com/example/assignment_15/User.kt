package com.example.assignment_15

data class User(
    var firstName:String?,
    var lastName:String?,
    var email:String?,
    var age:Int?,
    var gender:String?,
    var phoneNumber:String?,
    var address:String?,
    var profileImage:String?,
)
