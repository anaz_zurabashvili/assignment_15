package com.example.assignment_15


import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.assignment_15.abstracts.BaseFragment
import com.example.assignment_15.databinding.FragmentEditProfileBinding
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditProfileFragment :
    BaseFragment<FragmentEditProfileBinding>(FragmentEditProfileBinding::inflate) {
    lateinit var userManager: UserManager

    override fun init() {
        userManager = UserManager(context)
        listeners()
    }

    private fun listeners() {
        binding.btnSave.setOnClickListener {
            saveC()
            findNavController().navigate(
                EditProfileFragmentDirections.actionEditProfileFragmentToProfileFragment()
            )
        }
    }

    private fun saveC() {
        //Gets the user input and saves it
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val age = if (binding.etAge.text.toString().isNotEmpty()) binding.etAge.text.toString()
            .toInt() else 0
        val email = binding.etEmail.text.toString()
        val gender = binding.etGender.text.toString()
        val profileImage = binding.tvProfileImage.text.toString()
        val address = binding.etAddress.text.toString()
        val phoneNumber = binding.etPhoneNumber.text.toString()

        //Stores the values
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(IO) {
//                write()
                userManager.storeUser(
                    User(
                        firstName = firstName,
                        lastName = lastName,
                        age = age,
                        email = email,
                        profileImage = profileImage,
                        address = address,
                        phoneNumber = phoneNumber,
                        gender = gender
                    )

                )
            }
        }
    }
}