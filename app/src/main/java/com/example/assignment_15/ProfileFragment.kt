package com.example.assignment_15

import android.util.Log.d
import android.widget.ImageView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.assignment_15.abstracts.BaseFragment
import com.example.assignment_15.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    lateinit var userManager: UserManager

    override fun init() {
        userManager = UserManager(context)
        observeData()
        listeners()
    }

    private fun listeners() {
        binding.btnChangeInfo.setOnClickListener {
            findNavController().navigate(
                ProfileFragmentDirections.actionProfileFragmentToEditProfileFragment()
            )
        }
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launch {
            userManager.getFromDataStore().catch { e ->
                d("catch", "$e")
            }.collect {
                withContext(Main) {
                    binding.tvName.text = "name - ${it.firstName}"
                    binding.tvLastName.text = "second - ${it.lastName}"
                    binding.tvEmail.text = "email ${it.email}"
                    binding.tvAge.text = "age - ${it.age.toString()}"
                    binding.tvPhoneNumber.text = "phoneNumber - ${it.phoneNumber}"
                    binding.tvGender.text = "gender - ${it.gender} "
                    binding.tvAddress.text = "address - ${it.address}"
                    d("it", "${it.profileImage}")
                    binding.ivProfileImage.setImageUrl(it.profileImage)
                }
            }
        }
    }
}

fun ImageView.setImageUrl(url: String?) {
    if (!url.isNullOrEmpty())
        Glide.with(context).load(url).placeholder(R.drawable.ic_launcher_foreground).into(this)
    else
        setImageResource(R.drawable.ic_launcher_foreground)
}
